package Exercise;

import java.io.FileWriter;
import java.io.IOException;

import com.google.gson.Gson;

public class ObjectToJson {

	public static void main(String[] args){
		Gson gson = new Gson(); 
		Details ed = createmyObject(); 
		System.out.println(ed); // java object created 
		String j = gson.toJson(ed);  //to convert to JSON
		System.out.println(j);
		
		
		try (FileWriter writer = new FileWriter("C:\\Users\\Acer\\Desktop\\Details.json")){
		gson.toJson(ed,writer); 
	} 
		catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private static Details createmyObject() {
		Details ed = new Details(); 
		ed.setName("James"); 
		ed.setAge(23);
		ed.setGender("Male"); 
		ed.setCity("Munich"); 
		ed.setCountry("Germany"); 
		
		return ed; 
	}


}

