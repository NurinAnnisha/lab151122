package Exercise;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

import com.google.gson.Gson;

public class JsonToObject {

	public static void main(String[] args) {
		Gson gson = new Gson(); 
		
		try(Reader read = new FileReader("C:\\Users\\\\Acer\\Desktop\\Details.json")) {
			Details ed = gson.fromJson(read, Details.class); 
			System.out.println(ed); 
		} 
		
		catch (IOException e) {
			e.printStackTrace(); 
		}

	}

}