package EmpDetails;

import java.io.FileWriter;
import java.io.IOException;

import com.google.gson.Gson;

public class ObjectToJSON {

	public static void main(String[] args){
		Gson gson = new Gson(); 
		EmpDetails ed = createmyObject(); 
		System.out.println(ed); // java object created 
		String j = gson.toJson(ed);  //to convert to JSON
		System.out.println(j);
		
		
		try (FileWriter writer = new FileWriter("C:\\Users\\Acer\\Desktop\\myfile.json")){
		gson.toJson(ed,writer); 
		//System.out.println("ok its done go and check your myfile.json");
	} 
		catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private static EmpDetails createmyObject() {
		EmpDetails ed = new EmpDetails(); 
		ed.setName("Razak"); 
		ed.setAge(44); 
		ed.setCity("Perth"); 
		return ed; 
	}


}
