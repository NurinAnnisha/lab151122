package EmpDetails;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

import com.google.gson.Gson;

public class JsonToObject {

	public static void main(String[] args) {
		Gson gson = new Gson(); 
		
		try(Reader read = new FileReader("C:\\Users\\\\Acer\\Desktop\\myfile.json")) {
			EmpDetails ed = gson.fromJson(read, EmpDetails.class); 
			System.out.println(ed); //prints address of object 
		} 
		
		catch (IOException e) {
			e.printStackTrace(); 
		}

	}

}
