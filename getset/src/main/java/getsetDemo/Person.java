package getsetDemo;

public class Person {
	private String myname; //to access this variable, this requires setters & getters 
	
	public String getMyname() { //getter method 
		return myname; 
	}
	
	public void setMyname(String newMyname) { //setter method 
		this.myname=newMyname; 
	}

}
