package getsetDemo;

public class getSetDemo {

	public static void main(String[] args) {
		Person p = new Person();
		//p.name = "Ricky";
		//System.out.println(p.name); //if variable is private, it will not visible 
		p.setMyname("Razak");
		System.out.println(p.getMyname()); //get the value with getter method 
		
	}

}
